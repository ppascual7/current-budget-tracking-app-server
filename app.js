const connectToDb = require('./config/connectToDb')
const express = require('express') 
const dotenv = require('dotenv')
const usersRouter = require('./router/user')
const cors = require('cors')

dotenv.config({path: './config/.env'})

const app = express();

//body parser
app.use(cors());
app.options('*', cors())

app.use(express.json())

connectToDb();

app.use('/api/users', usersRouter)

//error handling middleware
app.use((err, req, res, next) => {
    res.status(400).send({
        error: {
            message: err.message
        }
    })
})

const port = process.env.PORT

app.listen( port , () => {
    console.log(`Running on port ${port}`)
})