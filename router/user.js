const router = require('express').Router();
const userController = require('./../controller/user');
const userIncomeController = require('./../controller/userIncome');
const userExpenseController = require('./../controller/userExpense')
const auth = require('./../auth')



router.post('/register',userController.register)

router.post('/login', userController.login)

router.get('/details', auth.verify, userController.getDetails)

router.get('/records', auth.verify, userController.getAllRecords)

router.post('/verify-google-id-token', async (req,res) => {

	console.log("HERE", req.body.tokenId)

	res.send(await userController.verifyGoogleTokenId(req.body.tokenId))
	
})
// Income Routes =====

router.post('/addIncomeCategory', auth.verify,userIncomeController.addCategory)

router.post('/addIncomeEntry', auth.verify,userIncomeController.addEntry)

router.delete('/removeIncomeCategory', auth.verify, userIncomeController.removeCategory)

router.delete('/removeIncomeEntry', auth.verify, userIncomeController.removeEntry)

router.post('/getIncomeCategories', auth.verify, userIncomeController.getCategories)

// Expense Routes =====

router.post('/addExpenseCategory', auth.verify, userExpenseController.addCategory)

router.post('/addExpenseEntry', auth.verify, userExpenseController.addEntry)

router.delete('/removeExpenseCategory', auth.verify, userExpenseController.removeCategory)

router.delete('/removeExpenseEntry', auth.verify, userExpenseController.removeEntry)

router.post('/getExpenseCategories', auth.verify, userExpenseController.getCategories)

module.exports = router;