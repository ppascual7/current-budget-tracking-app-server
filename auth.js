const jwt = require('jsonwebtoken')

module.exports.createAccessToken = (registeredUser) => {
    return jwt.sign({ 
        id: registeredUser._id,
        email: registeredUser.email,
     }, process.env.SECRET_KEY);
}

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    if( typeof token !== 'undefined'){
        token = token.split(" ")[1]

        jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
            if (!err) req.decodedToken = decoded
            
            return err ? res.send({ auth: "failed" }) : next()
        })

    } else {
        return res.send({ auth: "failed" })
    }
}