const mongoose = require('mongoose')
const dotenv = require('dotenv')

//Mock commit to change commit message

const connectToDB = () => {

     mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    });
    
    mongoose.connection.once('open', () => {
        console.log(`Connected to Database!`)
    })
    mongoose.connection.on('error', (error) => {
        console.log(`Connection Error: ${error}`)
    })

}

module.exports = connectToDB