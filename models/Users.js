const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: [true, "Email already in use"]
    },
    password: {
        type: String,
        minLength: 8,
    },

    loginType: {
        type: String,
        required: true
    },

    income: [
                {
                    categoryName: {
                        type: String,
                        required: true
                    },
                    incomeEntries: [
                        {
                            dateCreated: {
                                type: Date,
                                default: new Date()
                            },
                            amount: {
                                type: Number,
                                required: true
                            },
                            description: {
                                type: String,
                            }
                        }
                    ]
                }
            ],
    expense: [
        {
            categoryName: {
                type: String,
                required: true
            },
            expenseEntries: [
                {
                    dateCreated: {
                        type: Date,
                        default: new Date()
                    },
                    amount: {
                        type: Number,
                        required: true
                    },
                    description: {
                        type: String,
                    }
                }
            ]
        }
    ]
});

module.exports = mongoose.model('User', userSchema)
