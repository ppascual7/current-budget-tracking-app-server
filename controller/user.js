const User = require('./../models/Users')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const auth = require('./../auth')
const { OAuth2Client } = require('google-auth-library')
const clientId = "140527931092-ghc366f97h0it0q6v8b2vqt1nevnv8q2.apps.googleusercontent.com"

const register = (req, res, next) => {

    if(req.body.password !== req.body.confirmPassword){
        return res.status(400).send({
            error: {
                message: "Password Mismatch"
            }
        })
    }     

    // PASSWORD ENCRYPTION
    bcrypt.hash(req.body.password, 5, function(err, hash){

        req.body.password = hash;
        req.body.loginType = "email";

        User.create(req.body)
        .then ( user => {
            res.send(true)
        })
        .catch( next )
    })

}

const verifyGoogleTokenId = async (tokenId) => {

	console.log("From the User Controller:", tokenId)

	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	});

	if(data.payload.email_verified){
	
		const user = await User.findOne({email: data.payload.email})

		if (user !== null) {
			
			if (user.loginType === "google") {
				return { accessToken: auth.createAccessToken(user.toObject())}
			} else {
				return { error: "login-type-error" }
			}

		} else {

			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: "google"
			})

			return newUser.save().then((user, error) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			})

		}
		
	} else {

		return { error: "google-auth-error" };

	}


}


const login = (req, res, next) => {

    User.findOne({email: req.body.email})
    .then( user => {

        bcrypt.compare(req.body.password, user.password, (err, result) => {
            if(err) next()

            if(!result) {
                return res.status(400).send({ 
                    error: {
                        message: "Log in Failed"
                    }
                })
            } 

            let accessToken = auth.createAccessToken(user)

            res.send({ accessToken: accessToken})
        })
    })
    .catch( next )
}

const getDetails = (req, res, next) => {

    User.findById(req.decodedToken.id,{income:0, expense:0})
    .then(user => {
        if (!user) return res.send({error:{message: "User not found"}})
        res.send(user)
    })
    .catch(next)
    
}

const getAllRecords = (req, res, next) => {

    User.findById(req.decodedToken.id,{_id:0, firstName:0, lastName:0, email:0, password:0})
    .then(user => {
        if (!user) return res.send({error:{message: "User not found"}})
        res.send(user)
    })
    .catch(next)
    
}

module.exports = {
    register,
    login,
    getDetails,
    getAllRecords,
    verifyGoogleTokenId
}