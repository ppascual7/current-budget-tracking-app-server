const User = require('../models/Users')
const jwt = require('jsonwebtoken')

//TODO: ADD ROUTER LEVEL MIDDLEWARE (AUTH)
// Verify if categoryName already exists
const addCategory = (req, res, next) => {

    if(req.body.categoryName === undefined) return res.status(400).send({error:{message: "categoryName is Required"}})

    const category = {
        categoryName: req.body.categoryName,
        expenseEntries: []
    }

    let query = { 
        _id: req.body._id,
        "expense.categoryName": req.body.categoryName
    }
    let fieldProjection = {
        _id: 0, 
        'expense.$': 1
    }
    User.findOne(query, fieldProjection)
    .then(result => {

        if(result !== null) return res.status(400).send({error: {message: "Category already exists"}})

        User.findByIdAndUpdate(req.body._id, {
            $push: {
                expense: category
            }
        }, {new: true})
        .then ( result => {
            if(result === null) return res.send({error: {message: "Invalid User Id"}})
            res.send(true)
        })
        .catch( next )
    })
    .catch(next)
}

const addEntry = (req, res, next) => {

    const expenseEntry = {
        amount: req.body.amount,
        description: req.body.description
    }

    User.findOneAndUpdate({"expense._id":req.body.categoryId},
        {
            $push: {
                "expense.$.expenseEntries": expenseEntry
            }
        },{new: true})
    .then(result => {
        if(result === null) return res.send({error: {message: "Invalid Category Id"}})
        res.send(true)
    })
    .catch(next)
}

const removeCategory = (req, res, next) => {

    User.findOneAndUpdate({"expense._id":req.body.categoryId}, {
        $pull: {
            expense: {
                _id: req.body.categoryId
            }
        }
    },{new: true})
    .then(result => {
        if(result === null) return res.send({error: {message: "Invalid Category Id"}})
        res.send(true)
    })
    .catch( next )

}

const removeEntry = (req, res, next) => {

    User.findOneAndUpdate({"expense.expenseEntries._id": req.body.entryId}, {
        $pull: {
            "expense.$.expenseEntries": {
                    _id: req.body.entryId
            }
        }
    }, {new: true})
    .then(result => {
        if(result === null) return res.send({error: {message: "Invalid Entry Id"}})
        res.send(true)
    })
    .catch(next)
}

const getCategories = (req, res, next) => {

    let query = { 
        _id: req.body._id,
        "expense.categoryName": req.query.categoryName
    }
    let fieldProjection = {
        _id: 0, 
        'expense.$': 1
    }

    if(Object.keys(req.query).length === 0 && req.query.constructor === Object) {
        query = {_id: req.body._id}
        fieldProjection = {expense: 1, _id: 0}
    }

    User.findOne(query, fieldProjection)
    .then(result => {
        if(result === null) return res.send({error: {message: "Invalid Category Name or User Id"}})
        res.send(result)
    })
    .catch(next)
}

module.exports = {
    addCategory,
    addEntry,
    removeCategory,
    removeEntry,
    getCategories,
};